<?php

namespace App\Repository;

use App\Entity\Todo;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method Todo|null find($id, $lockMode = null, $lockVersion = null)
 * @method Todo|null findOneBy(array $criteria, array $orderBy = null)
 * @method Todo[]    findAll()
 * @method Todo[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class TodoRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Todo::class);
    }

    /**
     * @param array $data
     *
     * @return Todo
     *
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function create(array $data)
    {
        $entity = new Todo();

        if (isset($data['title'])) {
            $entity->setTitle($data['title']);
        }
        if (isset($data['done'])) {
            $entity->setDone($data['done']);
        }

        $this->save($entity, true);

        return $entity;
    }

    /**
     * @param Todo $entity
     * @param array $data
     *
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function update(Todo $entity, array $data)
    {
        if (isset($data['title'])) {
            $entity->setTitle($data['title']);
        }
        if (isset($data['done'])) {
            $entity->setDone($data['done']);
        }

        $this->save($entity);
    }

    /**
     * @param int $id
     *
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function delete(int $id)
    {
        $this->getEntityManager()->remove($this->find($id));
        $this->getEntityManager()->flush();
    }

    /**
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function deleteAll()
    {
        $items = $this->findAll();

        foreach ($items as $item) {
            $this->getEntityManager()->remove($item);
        }

        $this->getEntityManager()->flush();
    }

    /**
     * @param Todo $entity
     * @param bool $refresh
     *
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    private function save(Todo $entity, bool $refresh = false)
    {
        $this->getEntityManager()->persist($entity);
        $this->getEntityManager()->flush($entity);

        if ($refresh) {
            $this->getEntityManager()->refresh($entity);
        }
    }
}
