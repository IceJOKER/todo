<?php

namespace App\Controller;

use App\Repository\TodoRepository;
use Doctrine\ORM\ORMException;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class TodoController extends AbstractController
{
    /** @var TodoRepository */
    private $todoRepository;

    /**
     * TodoController constructor.
     *
     * @param TodoRepository $todoRepository
     */
    public function __construct(TodoRepository $todoRepository)
    {
        $this->todoRepository = $todoRepository;
    }

    /**
     * @Route("/todo", methods={"POST"}, name="todoCreate")
     * @param Request $request
     *
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     */
    public function create(Request $request)
    {
        try {
            //todo use data mapper, validate. make code great again!
            $data = (array) json_decode($request->getContent(false));
            $entity = $this->todoRepository->create($data);
        } catch (\Exception $exception) {
            //todo log && return "human" answer
            throw new \Exception('Something went wrong');
        }

        return $this->json($entity);
    }

    /**
     * @Route("/todo/{id}", methods={"PATCH"}, requirements={"id": "\d+"}, name="todoUpdate")
     * @param int $id
     * @param Request $request
     *
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     * @throws \Exception
     */
    public function update(int $id, Request $request)
    {
        $entity = $this->todoRepository->find($id);

        if (!$entity) {
            throw $this->createNotFoundException();
        }

        try {
            //todo use data mapper, validate. make code great again!
            $data = (array) json_decode($request->getContent(false));
            $this->todoRepository->update($entity, $data);
        } catch (\Exception $exception) {
            //todo log && return "human" answer
            throw new \Exception('Something went wrong');
        }

        return $this->json($entity);
    }

    /**
     * @Route("/todo", methods={"GET"}, name="todoList")
     *
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     */
    public function index()
    {
        return $this->json($this->todoRepository->findAll());
    }

    /**
     * @Route("/todo/{id}", methods={"GET"}, requirements={"id": "\d+"}, name="todoView")
     * @param int $id
     *
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     */
    public function view(int $id)
    {
        $entity = $this->todoRepository->find($id);

        if (is_null($entity)) {
            throw $this->createNotFoundException();
        }

        return $this->json($entity);
    }

    /**
     * @Route("/todo/{id}", methods={"DELETE"}, requirements={"id": "\d+"}, name="todoDelete")
     * @param int $id
     *
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     */
    public function delete(int $id)
    {
        $success = true;

        try {
            $this->todoRepository->delete($id);
        } catch (ORMException $exception) {
            $success = false;
        }

        return $this->json(['success' => $success]);
    }
}
