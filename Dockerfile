FROM php:7.2.9-fpm

RUN apt-get update && apt-get install -y zlib1g-dev

RUN docker-php-ext-install zip

RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer

WORKDIR /usr/src/app

COPY . /usr/src/app
RUN PATH=$PATH:/usr/src/app/vendor/bin:bin

CMD php bin/console server:run 0.0.0.0:8080