<?php

namespace App\Tests\Controller;

use App\Entity\Todo;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\KernelBrowser;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

abstract class AbstractControllerTest extends WebTestCase
{
    /** @var KernelBrowser */
    private static $client;

    protected function setUp(): void
    {
        parent::setUp();

        self::$client = self::createClient();
    }

    /**
     * @param array $options
     * @param array $server
     *
     * @return KernelBrowser
     */
    protected static function createClient(array $options = [], array $server = [])
    {
        self::$client = parent::createClient($options, $server);

        return self::$client;
    }

    /**
     * @param string $uri
     *
     * @return \Symfony\Component\DomCrawler\Crawler
     */
    protected function GET(string $uri)
    {
        return self::$client->request('GET', $uri);
    }

    /**
     * @param string $uri
     * @param array $data
     *
     * @return \Symfony\Component\DomCrawler\Crawler
     */
    protected function POST(string $uri, array $data)
    {
        return self::$client->request('POST', $uri, [], [], [], json_encode($data));
    }

    /**
     * @param string $uri
     * @param array $data
     *
     * @return \Symfony\Component\DomCrawler\Crawler
     */
    protected function PATCH(string $uri, array $data)
    {
        return self::$client->request('PATCH', $uri, [], [], [], json_encode($data));
    }

    /**
     * @param string $uri
     *
     * @return \Symfony\Component\DomCrawler\Crawler
     */
    protected function DELETE(string $uri)
    {
        return self::$client->request('DELETE', $uri);
    }

    protected function truncateEntities()
    {
        $this->getEntityManager()->createQuery("DELETE FROM ".Todo::class)->execute();
    }

    protected function saveEntity($entity)
    {
        $em = $this->getEntityManager();
        $em->persist($entity);
        $em->flush();
    }

    /**
     * @return array
     */
    protected function getResponseAsArray()
    {
        $response = self::$client->getResponse()->getContent();

        return (array) json_decode($response, true);
    }

    /**
     * @param array $data
     * @param string $msg
     */
    protected function assertResponseJsonMatch(array $data, string $msg = "")
    {
        $this->assertArraySubset($data, $this->getResponseAsArray(), $msg);
    }

    /**
     * @return EntityManagerInterface
     */
    private function getEntityManager()
    {
        return self::$client->getContainer()->get('doctrine.orm.default_entity_manager');
    }
}
