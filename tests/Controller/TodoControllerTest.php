<?php

namespace App\Tests\Controller;

use App\Entity\Todo;
use Symfony\Bundle\MakerBundle\Str;

class TodoControllerTest extends AbstractControllerTest
{
    private $entity;

    protected function setUp(): void
    {
        parent::setUp();

        $this->truncateEntities();

        $this->entity = $this->createEntity();
        $this->saveEntity($this->entity);
    }

    protected function tearDown(): void
    {
        parent::tearDown();


    }

    public function testCreate()
    {
        $data = [
            'title' => Str::getRandomTerm(),
            'done' => (bool) mt_rand(0, 1),
        ];

        $this->POST('/todo', $data);

        $this->assertResponseIsSuccessful();
        $this->assertResponseJsonMatch($data);
    }

    public function testUpdate()
    {
        $data = [
            'title' => Str::getRandomTerm(),
            'done' => (bool) mt_rand(0, 1),
        ];

        $this->PATCH('/todo/'.$this->entity->getId(), $data);

        $this->assertResponseIsSuccessful();
        $this->assertResponseJsonMatch($data);
    }

    public function testList()
    {
        $this->GET('/todo');

        $this->assertResponseIsSuccessful();
        $this->assertResponseJsonMatch([
            $this->entity->jsonSerialize(),
        ]);
    }

    public function testView()
    {
        $this->GET('/todo/'.$this->entity->getId());

        $this->assertResponseIsSuccessful();
        $this->assertResponseJsonMatch($this->entity->jsonSerialize());
    }

    public function testDelete()
    {
        $this->DELETE('/todo/'.$this->entity->getId());

        $this->assertResponseIsSuccessful();
        $this->assertResponseJsonMatch(['success' => true]);
    }

    /**
     * @return Todo
     */
    private function createEntity(): Todo
    {
        $entity = new Todo();
        $entity->setTitle(Str::getRandomTerm());
        $entity->setDone(false);

        return $entity;
    }
}
